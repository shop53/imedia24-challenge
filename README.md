# iMedia24 Coding challenge

## Request & Response Examples

### API Resources

- [GET /products/{sku}](#get-magazines)
- [GET /getProducts](#get-magazinesid)
- [POST /addProduct](#post-magazinesidarticles)
- [PATCH /updateProduct](#post-magazinesidarticles)

### GET /products/{sku}

Example: localhost:8080/products/123

Response body:

    {
        "sku": "123",
        "name": "name2",
        "description": "desc3",
        "price": 1.2
    }
### GET /getProducts

Example: localhost:8080/getProducts

Response body:

    [
        {
            "sku": "123",
            "name": "product1",
            "description": "desc1",
            "price": 1.2
        },
        {
            "sku": "4567",
            "name": "product2",
            "description": "desc2",
            "price": 15.5
        }
    ]

### POST /addProduct

Example: Create – POST  localhost:8080/addProduct

Request body:

    {
        "sku": "123",
        "name": "product1",
        "description": "product description",
        "price": 1.2
    }

### PATCH /updateProduct

Example: Create – POST  localhost:8080/updateProduct

Request body:

    {
        "sku": "123",
        "name": "productU",
        "description": "product description updated",
        "price": 1.2
    }

