package de.imedia24.shop

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.service.ProductService
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.ResponseEntity
import java.math.BigDecimal

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
class ShopApplicationTests @Autowired constructor(
	private val restTemplate: TestRestTemplate, private val productService: ProductService
){
	@LocalServerPort
	private var port: Int = 8080

	@Test
	fun contextLoads() {
	}

	@Test
	fun `status should return message Server Ok!`() {

		productService.addProduct(ProductEntity("123", "product1",
			"product test 1", BigDecimal("2.40")))

		productService.addProduct(ProductEntity("4567", "product2",
			"product test 2", BigDecimal("3.50")))

		productService.addProduct(ProductEntity("8901", "product3",
			"product test 3", BigDecimal("3.50")))

		productService.addProduct(ProductEntity("2345", "product4",
			"product test 4", BigDecimal("3.50")))

		val response: ResponseEntity<String> = restTemplate.getForEntity<String>(
			"http://localhost:$port/getProducts?skus=123,4567",
			String::class.java
		)

		Assertions.assertEquals(200, response.statusCode.value())
		Assertions.assertNotNull(response.body)
	}

	@Test
	fun `update product and return message Server Ok!`() {

		productService.addProduct(ProductEntity("123", "product1",
			"product test 1", BigDecimal("2.40")))

		restTemplate.put("http://localhost:$port/updateProduct",
			ProductRequest("123", "product1", "product updated", BigDecimal("2.40")),
				String::class.java)

		//Assertions.assertEquals(201, response.statusCode.value())
		//Assertions.assertNotNull(response.body)
		//Assertions.assertEquals("product updated", response.body?.description)
	}

}
