package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductRequest
import de.imedia24.shop.domain.product.ProductRequest.Companion.toProduct
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ProductController(private val productService: ProductService) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        logger.info("Request for product $sku")

        val product = productService.findProductBySku(sku)

        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @GetMapping("/getProducts", produces = ["application/json;charset=utf-8"])
    fun getProducts(
        @RequestParam("skus") skusList: Array<String>
    ): ResponseEntity< List<ProductResponse?>> {
        logger.info("Request to add product ${skusList.toString()}")


        val product = productService.findBySkuIn(skusList)

        return if (product != null) {
            ResponseEntity.ok(product)
        }
        else {
            ResponseEntity.notFound().build()
        }
    }

    @PostMapping("/addProduct", produces = ["application/json;charset=utf-8"])
    fun addProduct(
        @RequestBody productRequest: ProductRequest
    ): ResponseEntity<ProductResponse> {
        logger.info("Request to add product ${productRequest.name}")

        var productEntity = productRequest.toProduct()

        productService.addProduct(productEntity)

        return ResponseEntity.status(HttpStatus.CREATED).build()
    }

    @PatchMapping("/updateProduct", produces = ["application/json;charset=utf-8"])
    fun updateProduct(
        @RequestBody productRequest: ProductRequest
    ): ResponseEntity<ProductResponse> {
        logger.info("Request to update product ${productRequest.name}")

        val product = productService.findProductBySku(productRequest.sku)

        return if(product == null) {
            ResponseEntity.notFound().build()
        } else {
            var productEntity = productRequest.toProduct()
            productService.updateProduct(productEntity)
            ResponseEntity.status(HttpStatus.CREATED).build()
        }
    }
}
