package de.imedia24.shop.service

import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.time.ZonedDateTime

@Service
class ProductService(private val productRepository: ProductRepository) {

    private val logger = LoggerFactory.getLogger(ProductService::class.java)!!

    fun findProductBySku(sku: String): ProductResponse? {
        var productEntity = productRepository.findBySku(sku)

        return productEntity?.toProductResponse()
    }

    fun findBySkuIn(skus: Array<String>): List<ProductResponse?>? {
        var productEntityList = productRepository.findBySkuIn(skus)
        var ProductResponseList = productEntityList?.map { productEntity ->  productEntity?.toProductResponse()}
        return ProductResponseList
    }

    fun addProduct(productEntity: ProductEntity): ProductResponse? {
        productEntity.createdAt = ZonedDateTime.now()
        productEntity.updatedAt = ZonedDateTime.now()
        var productEntity = productRepository.save(productEntity)

        return productEntity?.toProductResponse()
    }

    fun updateProduct(productEntity: ProductEntity): ProductResponse? {
        productEntity.updatedAt = ZonedDateTime.now()
        var productEntity = productRepository.save(productEntity)

        return productEntity?.toProductResponse()
    }
}
